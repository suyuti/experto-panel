import axios from 'axios'

let instance = axios.create({
    baseURL : `${process.env.REACT_APP_API_URL}/v2`
})

export default instance