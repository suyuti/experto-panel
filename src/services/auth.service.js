import axios from './axios.service'

const login = (userName, password) => {
    return axios.post(`/auth/login`, {userName, password})
    .then(user => {
        if (user.data) {
            localStorage.setItem('jwt-access-token', user.data.token)
            return true
        }
        return false
    })
}

const register = (userName, password) => {
    return axios.post(`/auth/register`, {userName, password})
    .then(user => {
        if (user.data.status) {
            localStorage.setItem('jwt-access-token', user.data.token)
        }
    })
}

const logout = async () => {
    localStorage.removeItem('jwt-access-token')
}

export default {
    login,
    logout,
    register
}