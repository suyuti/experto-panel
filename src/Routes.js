import React, { lazy, Suspense, Fragment } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { AnimatePresence, motion } from 'framer-motion';
import { ThemeProvider } from '@material-ui/styles';
import { ClimbingBoxLoader } from 'react-spinners';
import MuiTheme from './theme';
import {
  LeftSidebar,
  CollapsedSidebar,
  MinimalLayout,
  PresentationLayout
} from './layout-blueprints';

const DashboardSatis = lazy(() => import('./pages/home/DashboardSatis'))
const LoginPage = lazy(() => import('./pages/account/LoginPage'))

const PrivateRoute = ({ component: Component, layout: Layout, ...rest }) => {
  return <Route {...rest} render={props => (
    localStorage.getItem('jwt-access-token') ?
      <Layout {...props}>
        <Component {...props} />
      </Layout>
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
  )} />
}

const RouteWithLayout = ({ component: Component, layout: Layout, ...rest }) => {
  return <Route {...rest} render={props => (
      <Layout {...props}>
        <Component {...props} />
      </Layout>
  )} />
}



const Routes = () => {
  const location = useLocation();

  const pageVariants = {
    initial: {
      opacity: 0,
      scale: 0.99
    },
    in: {
      opacity: 1,
      scale: 1
    },
    out: {
      opacity: 0,
      scale: 1.01
    }
  };

  const pageTransition = {
    type: 'tween',
    ease: 'anticipate',
    duration: 0.4
  };

  const SuspenseLoading = () => {
    return (
      <Fragment>
        <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
          <div className="d-flex align-items-center flex-column px-4">
            <ClimbingBoxLoader color={'#5383ff'} loading={true} />
          </div>
          <div className="text-muted font-size-xl text-center pt-3">
            Please wait while we load the live preview examples
            <span className="font-size-lg d-block text-dark">
            </span>
          </div>
        </div>
      </Fragment>
    );
  };
  return (
    <ThemeProvider theme={MuiTheme}>
      <AnimatePresence>
        <Suspense fallback={<SuspenseLoading />}>
          <Switch>
            <RouteWithLayout path='/login' component={LoginPage} layout={MinimalLayout}/>
            <RouteWithLayout path='/' component={DashboardSatis} layout={LeftSidebar}/>
          </Switch>
        </Suspense>
      </AnimatePresence>
    </ThemeProvider>
  );
};

export default Routes;
