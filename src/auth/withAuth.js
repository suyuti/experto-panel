import React from 'react'

function withAuth(WrappedComponent) {
    return class extends React.Component {
        componentDidMount() {
            let token = localStorage.getItem('jwt-access-token')
            if (token) {
                console.log(`token var`)
            }
            else {
                this.props.history.push('/login')
                console.log(`token yok. Logine git`)
            }
        }

        render() {
            return <WrappedComponent {...this.props} />
        }
    }
}

export default withAuth